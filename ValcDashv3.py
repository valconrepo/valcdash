import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from ValcTrace import vFigure, pid_tuner
#from dash.dependencies import Output, Input, State, ALL, MATCH
from dash_extensions.enrich import Dash, Output, Input, State
from OPCUA_client import DataInterface
from flask import Flask

server = Flask(__name__)
app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP],
                suppress_callback_exceptions=True, server=server,
                meta_tags=[
                    {"name": "viewport", "content": "width=device-width, initial-scale=1"}
                ])

data_interface = DataInterface(1)

figure = vFigure(app=app, data_interface=data_interface, name='Main')
tuner = pid_tuner(app=app, name='first')

app.layout = html.Div(children=[dcc.Location(id='url', refresh=False), html.Div(id='div-render',
                                                                                children=figure.plot())])


@app.callback(Output('div-render', 'children'),
              [Input('url', 'pathname')])
def link_render(path):
    if path == '/traces':
        return figure.plot()
    elif path == '/tuner':
        return tuner.plot()
    else:
        return figure.plot()


if __name__ == '__main__':
    app.run_server(debug=False, host='0.0.0.0', port=80)
