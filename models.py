from control.matlab import *


class model:
    def __init__(self, name=None, input_name=None, td=None, g=None, t1=None, t2=None):
        self.s = tf('s')
        self.time_delay = td
        self.gain = g
        self.time_const_1 = t1
        self.time_const_2 = t2
        self.function()
        self.name = name
        self.input_name = input_name

    def step_response(self):
        self.function()
        return step(self.fun)

    def input_response(self, input, time_step):
        self.function()
        time_vec = [time_step * index for index in range(len(input))]
        return lsim(self.fun, input, time_vec)

    def bode(self):
        self.function()
        return bode(self.fun)

    def margin(self):
        self.function()
        [Gm, Pm, Wcg, Wcp] = margin(self.fun)
        return [Gm, Pm]

    def get_variables(self):
        return [var [1:] for var, value in vars(self).items() if var [0] == '_' and value is not None]

    @property
    def time_delay(self):
        return self._time_delay

    @time_delay.setter
    def time_delay(self, value):
        self._time_delay = value

    @property
    def gain(self):
        return self._gain

    @gain.setter
    def gain(self, value):
        self._gain = value

    @property
    def time_const_1(self):
        return self._time_const_1

    @time_const_1.setter
    def time_const_1(self, value):
        self._time_const_1 = value

    @property
    def time_const_2(self):
        return self._time_const_2

    @time_const_2.setter
    def time_const_2(self, value):
        self._time_const_2 = value


class first_order(model):
    def __init__(self, td=1, g=1, t1=1):
        super().__init__(td=td, g=g, t1=t1)

    def function(self):
        if hasattr(self, '_gain') and hasattr(self, '_time_const_1') and hasattr(self, '_time_delay'):
            [delay_num, delay_den] = pade(self._time_delay)
            self.fun = series(tf(delay_num, delay_den), self._gain / (1 + self._time_const_1 * self.s))


class PID(model):
    def __init__(self, p=1, i=0, d=0):
        self.p = p
        self.i = i
        self.d = d
        super().__init__()

    @property
    def p(self):
        return self._p

    @p.setter
    def p(self, value):
        self.function()
        self._p = value

    @property
    def i(self):
        return self._i

    @i.setter
    def i(self, value):
        self.function()
        self._i = value

    @property
    def d(self):
        return self._d

    @d.setter
    def d(self, value):
        self.function()
        self._d = value

    def function(self):
        if hasattr(self, 'p') and hasattr(self, 'i') and hasattr(self, 'd'):
            if self.i == 0:
                _i = 0
            else:
                _i = 1 / self.i
            self.fun = (self.p * self.s + _i + self.d * self.s * self.s) / self.s

    def type(self):
        _pid_type = ''
        if self.p != 0:
            _pid_type += 'P'
        if self.i != 0:
            _pid_type += 'I'
        if self.d != 0:
            _pid_type += 'D'
        return _pid_type


class closed_loop(model):
    def __init__(self, plant, controller):
        self.plant = plant
        self.controller = controller
        super().__init__()

    def function(self):
        self.fun = feedback(self.plant.fun, self.controller.fun)


class second_order(model):
    def __init__(self, td=1, g=1, t1=1, t2=1):
        super().__init__(td=td, g=g, t1=t1, t2=t2)

    def function(self):
        if hasattr(self, '_gain') and hasattr(self, '_time_const_1') and hasattr(self, '_time_const_2') and hasattr(
                self, '_time_delay'):
            [delay_num, delay_den] = pade(self._time_delay)
            self.fun = series(tf(delay_num, delay_den),
                              self._gain / ((1 + self._time_const_1 * self.s) * (1 + self._time_const_2 * self.s)))


class integrator(model):
    def __init__(self, td=1, g=1):
        super().__init__(td=td, g=g)

    def function(self):
        if hasattr(self, '_gain') and hasattr(self, '_time_delay'):
            [delay_num, delay_den] = pade(self._time_delay)
            self.fun = series(tf(delay_num, delay_den), self._gain / self.s)


class integrator_with_lag(model):
    def __init__(self, td=1, g=1, t1=1):
        super().__init__(td=td, g=g, t1=t1)

    def function(self):
        if hasattr(self, '_gain') and hasattr(self, '_time_const_1') and hasattr(self, '_time_delay'):
            [delay_num, delay_den] = pade(self._time_delay)
            self.fun = series(tf(delay_num, delay_den), self._gain / (self.s * (1 + self._time_const_1 * self.s)))
