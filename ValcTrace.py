import dash
# from dash.dependencies import Output, Input, State, ALL, MATCH
from dash_extensions.enrich import Dash, Output, Input, State, ALL, MATCH
import dash_core_components as dcc
import dash_html_components as html
import json
import dash_table
from models import *
from dash.exceptions import PreventUpdate
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import dash_bootstrap_components as dbc


class module:
    module_initialised = False

    def __init__(self, name, app):
        self.__class__.instances.append(self)
        self.name = name
        self.initialise_callbacks(app)

    @classmethod
    def initialise_callbacks(cls, app):
        if not cls.initialised:
            if app is not None:
                cls.app = app
                cls.callbacks()
                if not cls.module_initialised:
                    cls.global_callbacks()
                    module.module_initialised = True
                cls.configurator()
                cls.initialised = True
            else:
                raise ValueError('for the first instance of the class you have to define dash application')

    @classmethod
    def get_current_object(cls, id_name):
        input_id = cls.get_input_id()
        index = [object.name for object in cls.instances].index(json.loads(input_id) [id_name])
        current_object = cls.instances [index]
        return current_object

    @classmethod
    def get_all_instances(cls):
        return cls.instances

    @classmethod
    def get_input_id(cls):
        ctx = dash.callback_context
        input_id = str()
        for i, part in enumerate(ctx.triggered [0] ['prop_id'].split('.')):
            if i != len(ctx.triggered [0] ['prop_id'].split('.')) - 1:
                part = '.' + part if 'UNIT' in part else part
                input_id += part
        return input_id

    @classmethod
    def global_callbacks(cls):
        @cls.app.callback(
            Output("sidebar", "className"),
            [Input("sidebar-toggle", "n_clicks")],
            [State("sidebar", "className")],
        )
        def toggle_classname(n, classname):
            if n and classname == "":
                return "collapsed"
            return ""

        @cls.app.callback(
            Output("collapse", "is_open"),
            [Input("navbar-toggle", "n_clicks")],
            [State("collapse", "is_open")],
        )
        def toggle_collapse(n, is_open):
            if n:
                return not is_open
            return is_open

    @classmethod
    def get_model_object_by_name(cls, name):
        return next((model for model in cls.model_objects if model.name == name), None)

    @classmethod
    def callbacks(cls):
        # here goes all callbacks in subclass. Raise exception if subclass has no method implemented
        raise NotImplementedError

    @classmethod
    def configurator(cls):
        # here goes configurators in subclass. It is not necessary  to implement configurator to all subclasses,
        # therefore pass
        pass

    # various general elements applied to all modules
    @staticmethod
    def return_to_main():
        return dcc.Link('Main', href='/', className='m-3')

    @staticmethod
    def navbar():

        navbar = dbc.NavbarSimple(
            children=[

                dbc.NavItem(dbc.NavLink("ValcTrace", href="/traces")),
                dbc.NavItem(dbc.NavLink("ValcTune", href="/tuner")),
            ],
            brand='ValcDash',
            brand_href="#",
            color="#3396d3",
            dark=True,
        )

        return navbar


class vFigure(module):
    color = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22',
             '#17becf']
    data_interface = None

    instances = []

    tab_style = {
        'borderBottom': '1px solid #d6d6d6',
        'padding': '6px',
        'fontWeight': 'bold'
    }

    tab_selected_style = {
        'borderTop': '1px solid #d6d6d6',
        'borderBottom': '1px solid #d6d6d6',
        'backgroundColor': '#119DFF',
        'color': 'white',
        'padding': '6px'
    }

    model_objects = []
    c = []
    model_list_table_data = []
    selected_values = []
    dropdown_options = []
    initialised = False

    def __init__(self, data_interface, name, app=None):
        vFigure.data_interface = data_interface
        self.trend_list = []
        self.fig = go.Figure()
        self.xaxis_range_len = 60
        self.xaxis_range_bias = 0
        self.yaxis_d = ['y', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7', 'y8', 'y9', 'y10']
        self.freeze = True
        self.model_list = []
        self.model_names = []
        self.layout()
        super().__init__(name, app)

    def layout(self):

        self.fig.layout = dict(height=900, autosize=True,
                               legend=dict(orientation='h', x=0, y=1.2, )
                               )
        self.update_y_axis()
        self.update_x_axis()

    @classmethod
    def model_list_table(cls):
        columns = ["Model Name", 'Model Input', 'Model Type', 'Delete']
        table_header = [
            html.Thead(html.Tr([html.Th(col_name) for col_name in columns]))
        ]

        table_body = [
            html.Tbody([
                html.Tr([
                    html.Td(model.name),
                    html.Td(model.input_name),
                    html.Td(model.__class__.__name__),
                    html.Td(dbc.Button('X', id={'type': 'delete-model', 'model-name': model.name}))
                ]) for model in cls.model_objects
            ])
        ]

        return dbc.Table(table_header + table_body, bordered=True)

    @classmethod
    def configuration_table(cls):

        columns = ['Point Name', 'Fixed Axis', 'Axis Selection', 'Trace Selection']
        table_header = [
            html.Thead(html.Tr([html.Th(col_name) for col_name in columns]))
        ]
        table_body = [
            html.Tbody([
                html.Tr([
                    html.Td(point_name),
                    html.Td(dbc.Select(
                        value=str(cls.trace_by_tag(point_name).fig.layout [
                                      cls.trace_by_tag(point_name).axis_naming(point_name)].fixedrange),
                        id={'point-name': point_name, 'config-type': 'fixed-axis'},
                        options=[
                            {'label': 'True', 'value': 'True'},
                            {'label': 'False', 'value': 'False'},
                        ],
                        bs_size='sm')
                    ),
                    html.Td(dbc.Select(
                        value=cls.trace_by_tag(point_name).get_tag_by_axis(cls.trace_by_tag(point_name).yaxis_d [i]),
                        id={'point-name': point_name, 'config-type': 'c'},
                        options=[{'label': i, 'value': i} for i in cls.trace_by_tag(point_name).trend_list],
                        bs_size='sm'
                    )),
                    html.Td(dbc.Select(
                        value=cls.trace_by_tag(point_name).name,
                        id={'point-name': point_name, 'config-type': 'trace-selection'},
                        options=[{'label': figure.name, 'value': figure.name} for figure in cls.instances],
                        bs_size='sm'
                    ))
                ]) for i, point_name in enumerate(cls.selected_values)
            ])
        ]

        return dbc.Table(table_header + table_body, bordered=True)

    @classmethod
    def get_figure_by_name(cls, name):
        return next((figure for figure in cls.instances if figure.name == name), None)

    @classmethod
    def trace_by_tag(cls, point_name):
        fig_list = [figure for figure in cls.instances if point_name in figure.trend_list]
        figure = fig_list [0] if fig_list != [] else cls.instances [0]
        return figure

    @classmethod
    def callbacks(cls):
        def update_data(i, tag, figure):
            model_names = [model.name for model in figure.model_list]
            if tag in model_names:
                y = figure.update_model(tag)
                figure.fig.data [i].y = y
            else:
                figure.fig.data [i].y = figure.data_interface.data [tag]
            figure.fig.data [i].x = figure.data_interface.data ['time']
            figure.fig.data [i].yaxis = figure.yaxis_d [i]
            figure.fig.data [i].name = tag

        '''
        @cls.app.callback(Output({'type': 'live-graph', 'figure': ALL}, 'figure'),
                          [Input('config-table-div', 'children')], group="figures")
        def graph_update(fig):
            figs = cls.get_all_instances()
            for figure in figs:
                for i, point_name in enumerate(figure.trend_list):
                    update_data(i, point_name, figure)
            return [figure.fig for figure in figs]
        '''

        @cls.app.callback(Output({'type': 'live-graph', 'figure': MATCH}, 'figure'),
                          [
                              # Input({'type': 'graph-update', 'figure': MATCH}, 'n_intervals'),
                              Input({'type': 'live-graph', 'figure': MATCH}, 'relayoutData'),
                              Input({'type': 'model-configurator', 'model': ALL, 'figure': MATCH, 'var': ALL}, 'value'),
                              Input({'type': 'import-data-button', 'figure': MATCH}, 'n_clicks')
                          ], group="figures"
                          )
        def live_graph(fig, model_data, click):
            current_figure = cls.get_current_object('figure')
            input_id = cls.get_input_id()
            data_return = False
            if 'import-data-button' in input_id:
                for i, point_name in enumerate(current_figure.trend_list):
                    update_data(i, point_name, current_figure)
                    cls.update_x_axis(current_figure)
                    cls.update_y_axis(current_figure)
                data_return = True
            if 'graph-update' in input_id and not current_figure.freeze and False:
                for i, point_name in enumerate(current_figure.trend_list):
                    update_data(i, point_name, current_figure)
                current_figure.update_x_axis()
                data_return = True
            '''
            if 'live-graph' in input_id and False:
                for i, point_name in enumerate(current_figure.trend_list):
                    yaxis_name = current_figure.axis_naming(point_name)
                    y_range = list(current_figure.fig.layout [f'{yaxis_name}.range'])
                    if f'{yaxis_name}.range[0]' in fig:
                        y_range [0] = fig [f'{yaxis_name}.range[0]']
                    if f'{yaxis_name}.range[1]' in fig:
                        y_range [1] = fig [f'{yaxis_name}.range[1]']
                    current_figure.fig.layout [f'{yaxis_name}.range'] = tuple(y_range)
                if 'xaxis.range' in fig and current_figure.freeze:
                    x_range = fig [f'xaxis.range']
                    current_figure.xaxis_range_len = x_range [1] - x_range [0]
            '''
            if 'model' in input_id:
                input_dict = json.loads(input_id)
                model_index = current_figure.model_names.index(input_dict ['model'])
                var_name = input_dict ['var']
                mod = current_figure.model_list [model_index]
                var_index = mod.get_variables().index(var_name)
                setattr(mod, var_name, model_data [len(mod.get_variables()) * model_index + var_index])
                point_index = next(
                    (index for (index, d) in enumerate(current_figure.fig.data) if
                     d ['name'] == input_dict ['model']),
                    None)
                current_figure.fig.data [point_index].y = current_figure.update_model(mod.name)
                current_figure.fig.data [point_index].x = current_figure.data_interface.data ['time']
                data_return = True

            if data_return:
                return current_figure.fig
            else:
                raise PreventUpdate

        @cls.app.callback(Output({'type': 'graph-update', 'figure': MATCH}, 'interval'),
                          [Input({'type': 'freeze-button', 'figure': MATCH}, 'n_clicks')])
        def update_button_output(n_clicks):
            current_figure = cls.get_current_object('figure')
            if not current_figure.freeze and cls.data_interface.OPC_client.is_connected():
                current_figure.freeze = True
                return 60 * 60 * 1000
            else:
                current_figure.freeze = False
                return 1 * 1000

        @cls.app.callback(Output('config-table-div', 'children'),
                          [Input('point-list-dropdown', 'value'),
                           Input({'point-name': ALL, 'config-type': ALL}, 'value')])
        def update_table(value, dynamic_value):
            def add_point(point_name, current_figure):
                # add point to correct figure and delete from everywhere else
                if point_name in model_names:
                    model_index = model_names.index(point_name)
                    current_figure.add_model(cls.model_objects [model_index])
                    for figure in cls.instances:
                        if figure is not current_figure:
                            figure.delete_model(cls.model_objects [model_index])
                current_figure.add_point(point_name)
                for figure in cls.instances:
                    if figure is not current_figure:
                        figure.delete_point(point_name)

            def delete_point(point_name):
                for figure in cls.instances:
                    figure.delete_point(point_name)
                    if point_name in model_names:
                        model_index = model_names.index(point_name)
                        figure.delete_model(cls.model_objects [model_index])

            input_id = cls.get_input_id()
            model_names = [mod.name for mod in cls.model_objects]
            full_list = cls.data_interface.point_list + model_names
            if input_id == 'point-list-dropdown':
                for point_name in value:
                    if point_name not in cls.selected_values:
                        add_point(point_name, cls.instances [0])
                for point_name in cls.selected_values:
                    if point_name not in value:
                        delete_point(point_name)
                cls.selected_values = value
            else:
                config_type = json.loads(input_id) ['config-type']
                point_name = json.loads(input_id) ['point-name']
                current_figure = cls.trace_by_tag(point_name)
                point_index = cls.selected_values.index(point_name)
                if config_type == 'fixed-axis':
                    yaxis_name = current_figure.axis_naming(point_name)
                    current_figure.fig.layout [yaxis_name].fixedrange = True if dynamic_value [
                                                                                    0 + 3 * point_index] == 'True' else False
                if config_type == 'axis-selection':
                    point_index_sel = current_figure.trend_list.index(point_name)
                    axis_tag = dynamic_value [1 + 3 * point_index]
                    current_figure.yaxis_d [point_index_sel] = current_figure.axis_naming_ind(axis_tag)
                    current_figure.fig.data [point_index_sel].yaxis = current_figure.yaxis_d [point_index_sel]
                if config_type == 'trace-selection':
                    current_figure = cls.get_figure_by_name(dynamic_value [2 + 3 * point_index])
                    add_point(point_name, current_figure)

            for figure in cls.instances:
                figure.update_trend_list()

            return cls.configuration_table()

        @cls.app.callback(Output('config-table-store', 'data'),
                          [Input('config-table', 'data')])
        def store_config_table_data(data):
            return data

        @cls.app.callback(
            [Output('model-list-table-div', 'children'), Output('model-input', 'value'),
             Output('model-dropdown', 'value')],
            [Input('add-model', 'n_clicks'), Input({'type': 'delete-model', 'model-name': ALL}, 'n_clicks')],
            [State('model-dropdown', 'value'),
             State('model-input', 'value'),
             State('model-name', 'value')])
        def model_input(clicks_add, clicks_delete, model, model_input, model_name):
            input_id = cls.get_input_id()
            if input_id == 'add-model':
                if model is None or model_input is None or model_name is None:
                    raise PreventUpdate
                model_obj = first_order()
                if model == 'first order':
                    model_obj = first_order()
                if model == 'second order':
                    model_obj = second_order()
                if model == 'integrator':
                    model_obj = integrator()
                if model == 'integrator with lag':
                    model_obj = integrator_with_lag()
                model_obj.name = model_name
                model_obj.input_name = model_input
                cls.dropdown_options.append({'label': model_name, 'value': model_name})
                cls.model_objects.append(model_obj)
            if 'delete-model' in input_id:
                model_name = json.loads(input_id) ['model-name']
                cls.model_objects = [model_obj for model_obj in cls.model_objects if model_obj.name != model_name]
                cls.dropdown_options = [option for option in cls.dropdown_options if option ['label'] != model_name]
            pid_tuner.update_model_objects(cls.model_objects)
            return cls.model_list_table(), None, None

        @cls.app.callback(Output('model-input', 'options'),
                          [Input('point-list-dropdown', 'options')])
        def update_model_dropdown(options):
            return options

        @cls.app.callback(Output({'type': 'model-config-div', 'figure': MATCH}, 'children'),
                          [Input({'type': 'add-widget', 'figure': MATCH}, 'value')])
        def add_widgets(value):
            input_id = cls.get_input_id()
            figure_name = json.loads(input_id) ['figure']
            figure = cls.get_figure_by_name(figure_name)
            return [figure.model_configurator(cls.get_model_object_by_name(model_name)) for model_name in value]

        @cls.app.callback(Output({'type': 'add-widget', 'figure': ALL}, 'options'),
                          [Input('model-list-table-div', 'children')])
        def update_widget_options(child):
            return [[{'label': model.name, 'value': model.name} for model in cls.model_objects]
                    for figure in cls.instances]

        @cls.app.callback([Output('point-list-dropdown', 'options')],
                          [Input('upload-data', 'contents')])
        def opc_func(contents):
            cls.data_interface.read_data_offline(contents)
            cls.dropdown_options = [{'label': point_name, 'value': point_name} for point_name in
                                        cls.data_interface.point_list]

            return cls.dropdown_options

        @cls.app.callback(Output('tabs', 'children'),
                          [Input('add-trend', 'n_clicks'), Input('delete-trend', 'n_clicks')],
                          [State('trend-name', 'value')])
        def add_trend(clicks, del_click, value):
            input_id = cls.get_input_id()
            if value is not None:
                if input_id == 'add-trend':
                    cls(cls.data_interface, value)
                if input_id == 'delete-trend':
                    cls.instances = [figure for figure in cls.instances if figure.name != value]
            return cls.plot(with_tabs=False)

        @cls.app.callback(Output({'type': 'widget-collapse', 'figure': MATCH}, 'is_open'),
                          [Input({'type': 'show-widgets-button', 'figure': MATCH}, 'n_clicks')],
                          [State({'type': 'widget-collapse', 'figure': MATCH}, 'is_open')])
        def show_widgets(n, is_open):
            return not is_open if n else is_open

    @classmethod
    def configurator(cls):
        def model_selector():
            model_list = ['first order', 'second order', 'integrator', 'integrator with lag']
            model_list_dropdown = [{'label': name, 'value': name} for name in model_list]

            model_selector_object = html.Div([
                html.Div(id='model-list-table-div', children=cls.model_list_table(), className='m-3'),
                dbc.Form([
                    dbc.FormGroup([
                        dbc.Label('Model Name', html_for='model-name', className="mr-2"),
                        dbc.Input(
                            id='model-name',
                            placeholder='enter model name'),
                    ], className="w-25 mr-3"),
                    dbc.FormGroup([
                        dbc.Label('Model Input', html_for='model-input', className="mr-2"),
                        dbc.Select(
                            id='model-input',
                            options=cls.dropdown_options,
                            value=None, )
                    ], className="w-25 mr-3"),
                    dbc.FormGroup([
                        dbc.Label('Model type', html_for='model-dropdown', className="mr-2"),
                        dbc.Select(
                            id='model-dropdown',
                            options=model_list_dropdown,
                            value=None)
                    ], className="w-25 mr-3"),
                    dbc.Button('Add Model', id='add-model', color='primary')
                ], inline=True, className='ml-3 mr-3')
            ])

            return model_selector_object

        def dropdown_point_list():
            config_table = html.Div(id='config-table-div', children=cls.configuration_table())
            list = dcc.Dropdown(
                id='point-list-dropdown',
                options=[],
                multi=True,
                value=[],
                persistence=True,
                persistence_type='local',
                placeholder='select points to be added to trends')
            return html.Div([config_table, list], className='m-3')

        def opc_configurator():
            output = dbc.Form([
                dbc.FormGroup([
                    dbc.Label('OPC address', html_for='opc-address'),
                    dbc.Input(id='opc-address', debounce=True, persistence=True, persistence_type='local',
                              className='w-100 mr-3'),
                    dbc.FormFeedback('Connected!', valid=True),
                    dbc.FormFeedback('Disconnected!', valid=False)
                ]),
                dbc.Button('Connect', id='opc-connect',
                           disabled=cls.data_interface.OPC_client.is_connected(), color='primary', className='mr-3'),
                dbc.Button('Disconnect', id='opc-disconnect',
                           disabled=not cls.data_interface.OPC_client.is_connected(), color='primary',
                           className='mr-3'),
            ], inline=True, className='m-3')

            return output

        def trend_adder():
            adder = dbc.Form([
                dbc.FormGroup([
                    dbc.Label('Trend name', html_for='trend-name'),
                    dbc.Input(id='trend-name', placeholder='enter trend name here', className='w-100 mr-2'),
                    dbc.FormText('You can delete or add new trend here!')
                ]),
                dbc.Button('Add trend', id='add-trend', className='mr-2', color='primary'),
                dbc.Button('Delete trend', id='delete-trend', className='mr-2', color='primary')
            ], inline=True, className='m-3')
            return adder

        def uploader():
            upload = html.Div([
                dcc.Upload(dbc.Button('Upload File', className='m-3', color='primary'), id='upload-data',
                           multiple=True),
                html.Div(id='output-data-upload'),
            ])
            return upload

        def storages():
            return html.Div([dcc.Store(id='config-table-store', storage_type='session')])

        return dbc.Row(dbc.Col(dropdown_point_list())), \
               dbc.Row([dbc.Col(uploader()), dbc.Col(trend_adder())]), \
               dbc.Row(dbc.Col(model_selector())), \
               dbc.Row(dbc.Col(cls.return_to_main())), \
               storages()

    def page(self):
        page_layout = html.Div([
            dbc.Row([
                dbc.Col([
                    dcc.Graph(id={'type': 'live-graph', 'figure': self.name}, figure=self.fig, animate=True,
                              config=dict({'scrollZoom': True}), ),
                    dcc.Interval(
                        id={'type': 'graph-update', 'figure': self.name},
                        interval=60 * 60 * 1000,
                        n_intervals=1
                    ),
                    dcc.Store(id={'type': 'live-graph-store', 'figure': self.name}, storage_type='session')]),
            ]),
            dbc.Row([
                dbc.Col([
                    dbc.Button(
                        'Freeze',
                        id={'type': 'freeze-button', 'figure': self.name},
                        className='mr-2 d-inline',
                        color='primary'
                    ),
                    dbc.Button(
                        'Show widgets',
                        id={'type': 'show-widgets-button', 'figure': self.name},
                        className='mr-2 d-inline',
                        color='primary'
                    ),
                    dbc.Button(
                        'Import data',
                        id={'type': 'import-data-button', 'figure': self.name},
                        className='mr-2 d-inline',
                        color='primary'
                    ),
                ], width=6)
            ], className='m-3'),
            dbc.Row([
                dbc.Col(
                    dbc.Collapse(id={'type': 'widget-collapse', 'figure': self.name},
                                 children=[
                                     html.Div(id={'type': 'model-config-div', 'figure': self.name}),
                                     dcc.Dropdown(children='Add widget',
                                                  id={'type': 'add-widget', 'figure': self.name},
                                                  multi=True, className='m-3')

                                 ]),
                    width=6
                )
            ], className='m-3')
        ])
        return page_layout

    def model_configurator(self, model):
        columns = ["Variable", 'Value']
        table_header = [
            html.Thead(html.Tr([html.Th(col_name) for col_name in columns]))
        ]

        table_body = [
            html.Tbody([
                html.Tr([
                    html.Td(var_name),
                    html.Td(dbc.Input(value=getattr(model, var_name), size='sm', type='number', debounce=True,
                                      id={'type': 'model-configurator', 'model': model.name, 'figure': self.name,
                                          'var': var_name})),
                ]) for var_name in model.get_variables()
            ])
        ]

        return dbc.Table(table_header + table_body, bordered=True, size='sm', className='m-3 d-inline')

    def update_y_axis(self):
        sides = ['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']
        positions = [0, 0.02, 0.04, 0.06, 0.08, 0.10, 0.12, 0.14, 0.16, 0.18]
        for index in range(10):
            axis_num_str = 'yaxis' if index == 0 else f'yaxis{index + 1}'
            self.fig.layout [axis_num_str] = dict(range=[0, 4], side=sides [index],
                                                  tickfont=dict(color=self.color [index]),
                                                  autorange=False, fixedrange=False,
                                                  position=positions [index])
            if index is not 0:
                self.fig.layout [axis_num_str].overlaying = 'y'

    def update_x_axis(self):
        max_points = min(self.xaxis_range_len, len(self.data_interface.data ['time']))
        self.fig.layout ['xaxis'] = dict(
            range=[self.data_interface.data ['time'] [0], self.data_interface.data ['time'] [-1]],
            autorange=True, fixedrange=False, domain=[0.1, 1],
            rangeslider=dict(visible=True,
                             range=[self.data_interface.data ['time'] [0], self.data_interface.data ['time'] [-1]]))

    def update_trend_list(self):
        i = 0
        full_list = self.data_interface.point_list + self.model_names
        for tag in full_list:
            point_index = next((index for (index, d) in enumerate(self.fig.data) if d ['name'] == tag), None)
            if self.trend_list is not None:
                if tag in self.trend_list and point_index is None:
                    index_in_trend_list = self.trend_list.index(tag)
                    yaxis = self.yaxis_d [index_in_trend_list]
                    y = self.update_model(tag) if tag in self.model_names else self.data_interface.data [tag]
                    self.fig.add_scatter(x=self.data_interface.data ['time'], y=y,
                                         name=tag, yaxis=yaxis,
                                         line=dict(color=self.color [index_in_trend_list]))
                if tag not in self.trend_list and point_index is not None:
                    self.fig.data = [data for data in self.fig.data if data.name != tag]
            i = i + 1

    def axis_naming(self, tag):
        index = self.trend_list.index(tag)
        if index is not 0:
            index = index + 1
        else:
            index = ''
        return f"yaxis{index}"

    def axis_naming_ind(self, tag):
        index = self.trend_list.index(tag)
        if index is not 0:
            index = index + 1
        else:
            index = ''
        return f"y{index}"

    def get_tag_by_axis(self, yaxis):
        index = min(int(yaxis [-1] if len(yaxis) >= 2 else 1), len(self.trend_list)) - 1
        return self.trend_list [index]

    def add_point(self, point):
        if point not in self.trend_list:
            self.trend_list.append(point)
            self.update_trend_list()

    def delete_point(self, point):
        if point in self.trend_list:
            self.trend_list = [name for name in self.trend_list if name != point]
            self.update_trend_list()

    def add_model(self, model):
        if model not in self.model_list:
            self.model_list.append(model)
            self.model_names = [mod.name for mod in self.model_list]
            if model.name not in self.trend_list:
                self.trend_list.append(model.name)
                self.update_trend_list()

    def delete_model(self, model):
        if model in self.model_list:
            if model.name in self.trend_list:
                self.trend_list = [name for name in self.trend_list if name != model.name]
                self.update_trend_list()
            self.model_names = [mod.name for mod in self.model_list]

    def update_model(self, tag):
        if tag in self.model_names:
            model_object = self.model_list [self.model_names.index(tag)]
            [y, t, x] = model_object.input_response(vFigure.data_interface.data [model_object.input_name], 1)
            return y
        else:
            return -1

    @classmethod
    def plot(cls, with_tabs=True):
        traces = [dbc.Tab(label=figure.name,
                          children=figure.page())
                  for figure in cls.instances] + [
                     dbc.Tab(label='Configuration', id='configurator',
                             children=cls.configurator())]
        traces_with_tabs = [cls.navbar(),
                            dbc.Tabs(id='tabs', persistence=True, persistence_type='session', children=traces)]
        return traces_with_tabs if with_tabs else traces


class pid_tuner(module):
    pid_columns = ['Model Name', 'Method', 'Labda', 'Proportional', 'Integral', 'Derivative',
                   'Max Disturbance',
                   'Phase Margin', 'Gain Margin']
    model_objects = []

    instances = []
    initialised = False

    def __init__(self, name, app=None):
        self.main = html.Div
        self.pid_config = {}
        self.pid_graph_data = {'y1': [0, 1], 'y2': [0, 1], 'y3': [0, 1], 'y4': [0, 1], 'x1': [0, 1], 'x2': [0, 1],
                               'x3': [0, 1], 'x4': [0, 1]}
        self.PID = PID()
        for column in pid_tuner.pid_columns:
            self.pid_config [column] = None
        self.pid_config ['Lambda'] = 1
        super().__init__(name=name, app=app)

    @classmethod
    def update_model_objects(cls, models):
        cls.model_objects = models

    @classmethod
    def callbacks(cls):
        @cls.app.callback([Output({'type': 'pid-tuning-table-div', 'name': MATCH}, 'children'),
                           Output({'type': 'pid-tuning-plots', 'name': MATCH}, 'figure')],
                          [Input({'type': 'pid-tuning-table-store', 'name': MATCH}, 'data')],
                          [State({'type': 'pid-tuning-plots', 'name': MATCH}, 'figure')])
        def update_pid_table(data, fig):
            current_object = cls.get_current_object('name')
            current_object.pid_config ['Lambda'] = data [0] ['Lambda']
            current_object.pid_config ['Method'] = data [0] ['Method']
            current_object.pid_config ['Model Name'] = data [0] ['Model Name']
            current_object.pid_plot_calculations()
            mod = next(
                (d for d in cls.model_objects if d.name == current_object.pid_config ['Model Name']),
                None)
            if mod is not None:
                current_object.pid_tuning_calculations(mod)
            for i in range(4):
                fig ['data'] [i] ['x'] = current_object.pid_graph_data [f'x{i + 1}']
                fig ['data'] [i] ['y'] = current_object.pid_graph_data [f'y{i + 1}']
            return [current_object.pid_tuning_table()], fig

        @cls.app.callback(Output({'type': 'pid-tuning-table-store', 'name': MATCH}, 'data'),
                          [Input({'type': 'pid-tuning-table', 'name': MATCH}, 'data')])
        def store_pid_table(data):
            return data

    @classmethod
    def get_object_by_name(cls, name):
        return next((cls.instances for object in cls.instances if object.name == name), None)

    def pid_tuning_table(self):
        methods = ['Lambda']
        presentations = ['dropdown', 'dropdown', 'input', 'input', 'input', 'input', 'input', 'input', 'input']

        table = dash_table.DataTable(
            id={'type': 'pid-tuning-table', 'name': self.name},
            data=[{column: self.pid_config [column] for column in pid_tuner.pid_columns}],
            columns=[{'name': column, 'id': column, 'presentation': presentation} for column, presentation in
                     zip(self.pid_columns, presentations)],
            dropdown={
                pid_tuner.pid_columns [0]: {
                    'options': [{'label': i.name, 'value': i.name} for i in self.model_objects]},
                pid_tuner.pid_columns [1]: {'options': [{'label': i, 'value': i} for i in methods]}},
            editable=True
        )
        return table

    def pid_tuning_plots(self):
        self.pid_plot_calculations()
        fig = make_subplots(rows=2, cols=2, subplot_titles=("Closed loop", "Open loop", "Gain", "Phase"))
        fig.layout.showlegend = False
        fig.layout.height = 700
        fig.add_trace(go.Scatter(y=self.pid_graph_data ['y1'], x=self.pid_graph_data ['x1']), row=1, col=1)
        fig.add_trace(go.Scatter(y=self.pid_graph_data ['y2'], x=self.pid_graph_data ['x2']), row=1, col=2)
        fig.add_trace(go.Scatter(y=self.pid_graph_data ['y3'], x=self.pid_graph_data ['x3']), row=2, col=1)
        fig.add_trace(go.Scatter(y=self.pid_graph_data ['y4'], x=self.pid_graph_data ['x4']), row=2, col=2)
        return dcc.Graph(id={'type': 'pid-tuning-plots', 'name': self.name}, figure=fig, animate=True)

    def pid_plot_calculations(self):
        mod = next((d for d in self.model_objects if d.name == self.pid_config ['Model Name']), None)
        if mod is not None:
            self.closed_loop_model = closed_loop(controller=self.PID, plant=mod)
            self.pid_tuning_calculations(mod)
            [y, t] = self.closed_loop_model.step_response()
            self.pid_graph_data ['y1'] = y
            self.pid_graph_data ['x1'] = t
            [y, t] = mod.step_response()
            self.pid_graph_data ['y2'] = y
            self.pid_graph_data ['x2'] = t
            [mag, phase, wout] = self.closed_loop_model.bode()
            self.pid_graph_data ['y3'] = mag
            self.pid_graph_data ['x3'] = wout
            self.pid_graph_data ['y4'] = phase
            self.pid_graph_data ['x4'] = wout

    def pid_tuning_calculations(self, mod):
        liambda = float(self.pid_config ['Lambda'])
        if mod.__class__.__name__ == 'first_order':
            self.PID.p = mod.time_const_1 / (mod.gain * (liambda + mod.time_delay))
            self.PID.i = mod.gain * (liambda + mod.time_delay)
        if mod.__class__.__name__ == 'second_order':
            self.PID.p = (mod.time_const_1 + mod.time_const_2) / (mod.gain * (liambda + mod.time_delay))
            self.PID.i = mod.gain * (liambda + mod.time_delay)
            self.PID.d = (mod.time_const_1 + mod.time_const_2) / (liambda + mod.time_delay)
        if mod.__class__.__name__ == 'integrator':
            self.PID.p = (2 * liambda + mod.time_delay) / (mod.gain * (liambda + mod.time_delay) ** 2)
            self.PID.i = mod.gain * (liambda + mod.time_delay) ** 2
        if mod.__class__.__name__ == 'integrator_with_lag':
            self.PID.p = (mod.time_const_1 + 2 * liambda + mod.time_delay) / (
                    mod.gain * (liambda + mod.time_delay) ** 2)
            self.PID.i = mod.gain * (liambda + mod.time_delay) ** 2
            self.PID.p = mod.time_const_1 * (2 * liambda + mod.time_delay) / (
                    mod.gain * (liambda + mod.time_delay) ** 2)
        self.pid_config ['Proportional'] = self.PID.p
        self.pid_config ['Integral'] = self.PID.i
        self.pid_config ['Derivative'] = self.PID.d
        self.closed_loop_model = closed_loop(controller=self.PID, plant=mod)
        [y, t] = self.closed_loop_model.step_response()
        self.pid_config ['Max Disturbance'] = (max(y)) * 100
        [Gm, Pm] = self.closed_loop_model.margin()
        self.pid_config ['Phase Margin'] = Pm
        self.pid_config ['Gain Margin'] = Gm

    def plot(self):
        return [self.navbar(),
                html.Div(
                    [html.Div(id={'type': 'pid-tuning-table-div', 'name': self.name}, children=self.pid_tuning_table()),
                     self.pid_tuning_plots(), self.return_to_main(),
                     dcc.Store(id={'type': 'pid-tuning-table-store', 'name': self.name}, storage_type='session')])]
