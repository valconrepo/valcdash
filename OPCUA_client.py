from opcua import Client
from datetime import datetime
import threading
import time
import sqlite3
import csv
import pandas as pd
import base64
import io
from dateutil import parser

class OPCUA_client():
    def __init__(self, address="opc.tcp://172.168.1.47:4840/freeopcua/server/"):
        self.address = address
        self.client = Client(self.address)
        self._connected = False

    def connect(self):
        self.client.connect()
        self.client.load_type_definitions()
        self.root = self.client.get_root_node()
        self.objects = self.client.get_objects_node()
        uri = "http://examples.freeopcua.github.io"
        self.idx = self.client.get_namespace_index(uri)
        self._connected = True

    def disconnect(self):
        self.client.disconnect()
        self._connected = False

    @property
    def address(self):
        return self._address

    @address.setter
    def address(self, value):
        self._address = value
        self.client = Client(self.address)

    def update_variable_list(self):
        if self.is_connected():
            self.objects.get_children()
            self.varlist = self.objects.get_children() [1:]

    def get_var_names(self):
        if self.is_connected():
            self.var_names = [node.get_browse_name().to_string() [2:] for node in self.varlist]
            return self.var_names
        else:
            return -1

    def get_value(self, MyVariable):
        if self.is_connected():
            value = self.root.get_child(
                ["0:Objects", f"2:{MyVariable}".format(self.idx), '2:Value'.format(self.idx)]).get_value()
            return value
        else:
            return -1

    def is_connected(self):
        try:
            if self._connected and self.client.uaclient._uasocket._thread.isAlive():
                return True
            else:
                return False
        except:
            return False


class DataInterface:
    def __init__(self, data_read_int):
        self.OPC_client = OPCUA_client()
        self.data_reading_thread = threading.Thread(target=self.read_data)
        self.point_list = []
        self.data = {}
        self.read_data_int = data_read_int
        self.data ['time'] = [datetime.now().strftime("%H:%M:%S")]
        self.run = False
        self.data_reading_thread.start()

    def start(self):
        self.run = True
        self.OPC_client.connect()
        self.update_point_list()
        for tag in self.point_list:
            self.data [tag] = [1]

    def stop(self):
        self.run = False
        self.OPC_client.disconnect()
        self.point_list = []
        self.data = {}
        self.data ['time'] = [datetime.now().strftime("%H:%M:%S")]

    def read_data(self):
        while True:
            start_time = time.time()
            if self.run:
                if self.OPC_client.is_connected():
                    for tag in self.point_list:
                        try:
                            self.data [tag].append(self.OPC_client.get_value(tag))
                        except:
                            self.update_point_list()
                    self.data ['time'].append(datetime.now().strftime("%H:%M:%S"))
            time.sleep(self.read_data_int - ((time.time() - start_time) % self.read_data_int))

    def update_point_list(self):
        self.OPC_client.update_variable_list()
        new_variables = self.OPC_client.get_var_names()
        for var in new_variables:
            if var not in self.point_list:
                self.data [var] = [0 for i in range(len(self.data ['time']))]
        for var in self.point_list:
            if var not in new_variables:
                del self.data [var]
        self.point_list = new_variables

    def read_data_offline(self, con_list):
        dft = [decodeOvcsv(con) for con in con_list]
        df = pd.concat(dft)
        self.update_point_list_offline(df)
        self.data['time'] = df.index
        for tag in self.point_list:
            self.data[tag] = df[tag]

    def update_point_list_offline(self, df):
        self.point_list = list(df)



class HistoryManager:
    def __init__(self):
        self._db_connection = sqlite3.connect('points_database.db')
        self._db_cur = self._db_connection.cursor()

    def query(self, query, params):
        return self._db_cur.execute(query, params)

    def __del__(self):
        self._db_connection.close()


def decodeOvcsv(f):
    content_type, content_string = f.split(',')
    decoded = base64.b64decode(content_string)

    # Find first row
    for n, row in enumerate(csv.reader(io.StringIO(decoded.decode('utf-8')))):
        if row != []:
            if row [0] == 'Date Time':
                break
    header = n - 4

    # Read csv file
    df = pd.read_csv(io.StringIO(decoded.decode('utf-8')), header=header).drop([0, 1])

    # Strings to Date type and set as index date
    df ['Date Time'] = [parser.parse(date) for date in df ['Date Time']]
    df = df.set_index('Date Time')

    # Remove unit and network suffix
    rd = {cn: cn.split('.') [0] for cn in list(df)}
    df = df.rename(columns=rd, inplace=False)

    # Remove poor and bad quality indications and convert to float
    df = df.replace({' B': ''}, regex=True)
    df = df.replace({' P': ''}, regex=True)
    df = df.astype(float)

    # sort values by date
    df = df.sort_values(by='Date Time')

    return df

if __name__ == '__main__':
    server = OPCUA_client()
    server.connect()
    server.update_variable_list()
    names = server.get_var_names()
    print(names)
    value = server.get_value(names [0])
    print(value)
    server.disconnect()
